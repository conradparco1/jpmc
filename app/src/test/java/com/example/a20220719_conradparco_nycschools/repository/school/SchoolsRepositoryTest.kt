package com.example.a20220719_conradparco_nycschools.repository.school

import com.example.a20220719_conradparco_nycschools.data.School
import io.mockk.*
import io.mockk.impl.annotations.MockK
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test

class SchoolRepositoryTest {

    @MockK
    private lateinit var schoolRemote: SchoolsRemoteSourceContract
    @MockK
    private lateinit var schoolLocal: SchoolsLocalSourceContract
    @MockK
    private lateinit var mockSchool: School
    @MockK
    private lateinit var mockSchool2: School
    private val dispatcher: CoroutineDispatcher by lazy { Dispatchers.Unconfined }
    private lateinit var repo: SchoolsRepository

    @Before
    fun setup() {
        MockKAnnotations.init(this)
        repo = SchoolsRepository(schoolLocal, schoolRemote, dispatcher)
        every { mockSchool.schoolName } returns "Zorro"
        every { mockSchool2.schoolName } returns "Alphabet"
    }

    @Test
    fun `fetch schools from remote sources and check that schools are persisted and returned sorted`() = runBlocking {
        // Given
        val mockFetchedSchools = listOf(mockSchool, mockSchool2)
        coEvery { schoolLocal.fetchSchools() } returns emptyList()
        coEvery { schoolRemote.fetchSchools() } returns mockFetchedSchools
        coEvery { schoolLocal.persistSchools(mockFetchedSchools) } just Runs

        // When
        val results = repo.fetchSchools()

        // Then
        coVerify { schoolLocal.persistSchools(mockFetchedSchools) }
        assertEquals(mockSchool2, results.first())
    }

    @Test
    fun `fetch schools from local sources when present and then verify they're sorted`() = runBlocking {
        // Given
        val mockFetchedSchools = listOf(mockSchool, mockSchool2)
        coEvery { schoolLocal.fetchSchools() } returns mockFetchedSchools

        // When
        val results = repo.fetchSchools()

        // Then
        assertEquals(mockSchool2, results.first())
        coVerify(exactly = 0){ schoolRemote.fetchSchools() }
    }
}