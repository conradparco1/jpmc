package com.example.a20220719_conradparco_nycschools.api

import com.example.a20220719_conradparco_nycschools.data.School
import retrofit2.http.GET

interface SchoolsApi {

    @GET("s3k6-pzi2.json")
    suspend fun fetchSchools(): List<School>
}