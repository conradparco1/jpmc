package com.example.a20220719_conradparco_nycschools.api

import com.example.a20220719_conradparco_nycschools.data.Score
import retrofit2.http.GET

interface ScoresApi {

    @GET("f9bf-2cp4.json")
    suspend fun fetchScores(): List<Score>
}