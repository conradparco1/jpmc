package com.example.a20220719_conradparco_nycschools.repository.scores

import com.example.a20220719_conradparco_nycschools.data.Score
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

// TODO replace with Room; WARNING this is in-mem cache only
class ScoresLocalSource(
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) : ScoresLocalSourceContract {

    private val scores = mutableListOf<Score>()

    override suspend fun persistScores(scores: List<Score>) {
        synchronized(this) {
            this.scores.clear()
            this.scores.addAll(scores)
        }
    }

    override suspend fun fetchScoresForSchool(dbn: String): Score? = withContext(dispatcher) {
        synchronized(this) {
            if (scores.isEmpty()) return@withContext null
            return@withContext scores.firstOrNull { it.dbn.equals(dbn, true) }
        }
    }

    override suspend fun fetchScores(): List<Score> = synchronized(this) { scores }
}