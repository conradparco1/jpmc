package com.example.a20220719_conradparco_nycschools.repository.school

import androidx.annotation.GuardedBy
import com.example.a20220719_conradparco_nycschools.data.School
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class SchoolsRepository(
    private val local: SchoolsLocalSourceContract,
    private val remote: SchoolsRemoteSourceContract,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) : SchoolsRepositoryContract {

    @GuardedBy("this")
    @Volatile
    private var cachedSchool: School? = null

    override fun setSelectedSchool(school: School) {
        synchronized(this) {
            cachedSchool = school
        }
    }

    override fun getSelectedSchool(): School? = synchronized(this) {
        cachedSchool
    }

    override fun clearSelectedSchool() {
        synchronized(this) {
            cachedSchool = null
        }
    }

    override suspend fun fetchSchools(): List<School> = withContext(dispatcher) {
        return@withContext fetchSchoolsFromSources().sortedBy { it.schoolName }
    }

    private suspend fun fetchSchoolsFromSources() = withContext(dispatcher) {
        val cachedSchools = local.fetchSchools()
        if (cachedSchools.isNotEmpty()) return@withContext cachedSchools
        val schools = remote.fetchSchools()
        if (schools.isNotEmpty()) local.persistSchools(schools)
        return@withContext schools
    }
}