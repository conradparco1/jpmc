package com.example.a20220719_conradparco_nycschools.repository.school

import com.example.a20220719_conradparco_nycschools.data.School

interface SchoolsDataSource {

    suspend fun fetchSchools(): List<School>
}

interface SchoolsRemoteSourceContract : SchoolsDataSource

interface SchoolsLocalSourceContract : SchoolsDataSource {

    suspend fun persistSchools(schools: List<School>)

    suspend fun clear()
}

interface SchoolsRepositoryContract : SchoolsDataSource {

    fun setSelectedSchool(school: School)

    fun getSelectedSchool(): School?

    fun clearSelectedSchool()
}