package com.example.a20220719_conradparco_nycschools.repository.school

import com.example.a20220719_conradparco_nycschools.api.SchoolsApi
import com.example.a20220719_conradparco_nycschools.data.School
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class SchoolsRemoteSource(
    private val api: SchoolsApi,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) : SchoolsRemoteSourceContract {

    override suspend fun fetchSchools(): List<School> = withContext(dispatcher) {
        api.fetchSchools()
    }
}