package com.example.a20220719_conradparco_nycschools.repository.scores

import com.example.a20220719_conradparco_nycschools.api.ScoresApi
import com.example.a20220719_conradparco_nycschools.data.Score
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

// TODO Write unit tests when you have time
class ScoresRepository(
    private val scoresApi: ScoresApi,
    private val localScores: ScoresLocalSourceContract,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) : ScoresDataSource {

    override suspend fun fetchScoresForSchool(dbn: String): Score? =
        withContext(dispatcher) {
            val localFoundScore = localScores.fetchScoresForSchool(dbn)
            when {
                localFoundScore != null -> return@withContext localFoundScore
                localScores.fetchScores().isNotEmpty() -> null
                else -> {
                    val scores = fetchScores()
                    localScores.persistScores(scores)
                    return@withContext scores.firstOrNull { it.dbn.equals(dbn, true) }
                }
            }
        }

    override suspend fun fetchScores(): List<Score> =
        withContext(dispatcher) { scoresApi.fetchScores() }
}