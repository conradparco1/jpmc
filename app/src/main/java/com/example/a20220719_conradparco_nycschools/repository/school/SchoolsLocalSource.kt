package com.example.a20220719_conradparco_nycschools.repository.school

import com.example.a20220719_conradparco_nycschools.data.School


// TODO Replace with ROOM when there is time; WARNING This is only in-mem cache
class SchoolsLocalSource : SchoolsLocalSourceContract {

    @Volatile
    private var cache = mutableListOf<School>()

    override suspend fun persistSchools(schools: List<School>) {
        synchronized(this) {
            cache.apply {
                clear()
                addAll(schools)
            }
        }
    }

    override suspend fun clear() {
        synchronized(this){
            cache.clear()
        }
    }

    override suspend fun fetchSchools(): List<School> = synchronized(this){
        cache
    }
}