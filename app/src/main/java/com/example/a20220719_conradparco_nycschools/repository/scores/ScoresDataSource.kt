package com.example.a20220719_conradparco_nycschools.repository.scores

import com.example.a20220719_conradparco_nycschools.data.Score

interface ScoresDataSource {

    suspend fun fetchScoresForSchool(dbn: String): Score?

    suspend fun fetchScores(): List<Score>
}

interface ScoresLocalSourceContract : ScoresDataSource {
    suspend fun persistScores(scores: List<Score>)
}