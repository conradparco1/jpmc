package com.example.a20220719_conradparco_nycschools.data

import com.google.gson.annotations.SerializedName

data class Score(
    val dbn: String,
    @SerializedName("school_name")
    val schoolName: String,
    @SerializedName("num_of_sat_test_takers")
    val numberOfSATTakers: String,
    @SerializedName("sat_critical_reading_avg_score")
    val averageCriticalReadingScore: String,
    @SerializedName("sat_math_avg_score")
    val averageMathScore: String,
    @SerializedName("sat_writing_avg_score")
    val averageWritingScore: String
)