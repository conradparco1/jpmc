package com.example.a20220719_conradparco_nycschools.di

import com.example.a20220719_conradparco_nycschools.repository.school.*
import com.example.a20220719_conradparco_nycschools.repository.scores.ScoresDataSource
import com.example.a20220719_conradparco_nycschools.repository.scores.ScoresLocalSource
import com.example.a20220719_conradparco_nycschools.repository.scores.ScoresLocalSourceContract
import com.example.a20220719_conradparco_nycschools.repository.scores.ScoresRepository


// TODO replace with DI framework
// Holds our application wide dependency graph
object AppModule {

    private val localSchools: SchoolsLocalSourceContract by lazy {
        SchoolsLocalSource()
    }

    private val remoteSchools: SchoolsRemoteSourceContract by lazy {
        SchoolsRemoteSource(NetworkingModule.schoolApi)
    }

    private val localScores: ScoresLocalSourceContract by lazy {
        ScoresLocalSource()
    }

    val schoolRepo: SchoolsRepositoryContract by lazy {
        SchoolsRepository(
            localSchools,
            remoteSchools
        )
    }

    val scoresRepo: ScoresDataSource by lazy {
        ScoresRepository(
            NetworkingModule.scoresApi,
            localScores
        )
    }
}