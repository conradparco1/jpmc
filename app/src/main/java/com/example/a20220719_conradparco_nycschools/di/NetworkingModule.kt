package com.example.a20220719_conradparco_nycschools.di

import com.example.a20220719_conradparco_nycschools.api.SchoolsApi
import com.example.a20220719_conradparco_nycschools.api.ScoresApi
import com.example.a20220719_conradparco_nycschools.data.School
import com.google.gson.Gson
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

// TODO Replace with Dagger or Hilt or Koin later
object NetworkingModule {

    private val gson: Gson by lazy { Gson() }


    // TODO add interceptor later
    private val okHttpClient: OkHttpClient by lazy {
        OkHttpClient.Builder()
            .build()
    }


    // TODO inject via gradle later
    private val url: String by lazy {
        "https://data.cityofnewyork.us/resource/"
    }

    private val retrofit: Retrofit by lazy {
        Retrofit.Builder()
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(url)
            .build()
    }

    val schoolApi: SchoolsApi by lazy {
        retrofit.create(SchoolsApi::class.java)
    }

    val scoresApi: ScoresApi by lazy {
        retrofit.create(ScoresApi::class.java)
    }
}