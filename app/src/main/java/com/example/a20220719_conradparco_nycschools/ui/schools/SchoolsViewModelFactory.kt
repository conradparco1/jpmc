package com.example.a20220719_conradparco_nycschools.ui.schools

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.a20220719_conradparco_nycschools.di.AppModule

class SchoolsViewModelFactory : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SchoolsViewModel::class.java)) {
            return SchoolsViewModel(AppModule.schoolRepo) as T
        }
        throw IllegalArgumentException("Must be assignable from SchoolsViewModel")
    }
}