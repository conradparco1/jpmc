package com.example.a20220719_conradparco_nycschools.ui.schools

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.a20220719_conradparco_nycschools.data.School

interface SchoolAdapterListener {
    fun onSchoolTapped(school: School)
}

class SchoolAdapter(
    private val listener: SchoolAdapterListener
) : RecyclerView.Adapter<SchoolViewHolder>(), SchoolViewHolderListener {

    private val items = mutableListOf<School>()

    fun addSchools(schools: List<School>) {
        this.items.clear()
        this.items.addAll(schools)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = SchoolViewHolder(parent, this)

    override fun onBindViewHolder(holder: SchoolViewHolder, position: Int) {
        holder.bindModel(items[position])
    }

    override fun getItemCount() = items.size

    override fun onSchoolTapped(position: Int) {
        listener.onSchoolTapped(items[position])
    }
}