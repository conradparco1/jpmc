package com.example.a20220719_conradparco_nycschools.ui.schooldetails

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.a20220719_conradparco_nycschools.di.AppModule

class SchoolDetailsViewModelFactory : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SchoolDetailsViewModel::class.java)) {
            return SchoolDetailsViewModel(AppModule.scoresRepo, AppModule.schoolRepo) as T
        }
        throw IllegalArgumentException("Must be assignable from SchoolsViewModel")
    }
}