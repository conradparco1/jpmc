package com.example.a20220719_conradparco_nycschools.ui.schools

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a20220719_conradparco_nycschools.data.ApiResponse
import com.example.a20220719_conradparco_nycschools.data.School
import com.example.a20220719_conradparco_nycschools.repository.school.SchoolsDataSource
import com.example.a20220719_conradparco_nycschools.repository.school.SchoolsRepositoryContract
import kotlinx.coroutines.launch

class SchoolsViewModel(
    private val schoolsRepo: SchoolsRepositoryContract
) : ViewModel() {

    private val _response = MutableLiveData<ApiResponse<List<School>>>()
    val response: LiveData<ApiResponse<List<School>>> get() = _response

    fun fetchSchools() {
        _response.postValue(ApiResponse.Loading())
        viewModelScope.launch {
            try {
                _response.postValue(ApiResponse.Success(schoolsRepo.fetchSchools()))
            } catch (e: Throwable) {
                _response.postValue(ApiResponse.Error(e))
            }
        }
    }

    fun onSchoolTapped(school: School) {
        schoolsRepo.setSelectedSchool(school)
    }
}