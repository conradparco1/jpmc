package com.example.a20220719_conradparco_nycschools.ui.schools

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.a20220719_conradparco_nycschools.data.School
import com.example.a20220719_conradparco_nycschools.databinding.RowSchoolItemBinding

interface SchoolViewHolderListener {
    fun onSchoolTapped(position: Int)
}

class SchoolViewHolder(
    private val parent: ViewGroup,
    private val listener: SchoolViewHolderListener,
    val binding: RowSchoolItemBinding = RowSchoolItemBinding.inflate(
        LayoutInflater.from(parent.context),
        parent,
        false
    )
) : RecyclerView.ViewHolder(binding.root) {

    fun bindModel(model: School) {
        binding.schoolTitle.text = model.schoolName
        binding.schoolDescription.text = model.location ?: ""
        binding.root.setOnClickListener {
            listener.onSchoolTapped(adapterPosition)
        }
    }
}