package com.example.a20220719_conradparco_nycschools.ui.schools

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.a20220719_conradparco_nycschools.BuildConfig
import com.example.a20220719_conradparco_nycschools.R
import com.example.a20220719_conradparco_nycschools.data.ApiResponse
import com.example.a20220719_conradparco_nycschools.data.School
import com.example.a20220719_conradparco_nycschools.databinding.FragmentSchoolsBinding
import com.example.a20220719_conradparco_nycschools.utils.gone
import com.example.a20220719_conradparco_nycschools.utils.show


class SchoolsFragment : Fragment(), SchoolAdapterListener {


    private var _binding: FragmentSchoolsBinding ? = null
    private val binding get() = _binding!!
    private val viewModel: SchoolsViewModel by lazy {
        ViewModelProvider(this, SchoolsViewModelFactory())[SchoolsViewModel::class.java]
    }
    private var schoolsAdapter: SchoolAdapter? = null
    private var cachedRecyclerPosition: Int = -1

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentSchoolsBinding.inflate(inflater, container, false).apply {
        _binding = this
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (savedInstanceState != null) {
            cachedRecyclerPosition = savedInstanceState.getInt(KEY_POSITION)
        }
        setViews()
        observe()
        viewModel.fetchSchools()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putInt(KEY_POSITION, cachedRecyclerPosition)
        super.onSaveInstanceState(outState)
    }

    override fun onStop() {
        cachedRecyclerPosition = (binding.recyclerView.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
        super.onStop()
    }

    override fun onDestroyView() {
        schoolsAdapter = null
        super.onDestroyView()
    }

    private fun setViews() {
        schoolsAdapter = SchoolAdapter(this)
        binding.recyclerView.apply {
            adapter = schoolsAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }
    }

    private fun observe() {
        viewModel.response.observe(viewLifecycleOwner) {
            when (it) {
                is ApiResponse.Success -> onSuccess(it)
                is ApiResponse.Error -> onError(it)
                is ApiResponse.Loading -> showLoading()
            }
        }
    }

    private fun onSuccess(success: ApiResponse.Success<List<School>>) {
        stopShowingLoading()
        schoolsAdapter?.addSchools(success.data)
        if (cachedRecyclerPosition != -1) {
            binding.recyclerView.scrollToPosition(cachedRecyclerPosition)
        }
    }

    private fun onError(error: ApiResponse.Error<List<School>>) {
        stopShowingLoading()
        Toast.makeText(requireContext(), getString(R.string.school_error), Toast.LENGTH_LONG).show()
        if (BuildConfig.DEBUG){
            Log.w(TAG, "Error: ${error.error?.message}")
        }
    }

    private fun showLoading() {
        binding.progressBar.show()
    }

    private fun stopShowingLoading() {
        binding.progressBar.gone()
    }

    companion object {
        private const val TAG = "SchoolsFragment"
        private const val KEY_POSITION = "position"
    }

    override fun onSchoolTapped(school: School) {
        viewModel.onSchoolTapped(school)
        findNavController().navigate(R.id.action_schools_to_details)
    }
}