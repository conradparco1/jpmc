package com.example.a20220719_conradparco_nycschools.ui.schooldetails

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a20220719_conradparco_nycschools.R
import com.example.a20220719_conradparco_nycschools.repository.school.SchoolsRepositoryContract
import com.example.a20220719_conradparco_nycschools.repository.scores.ScoresDataSource
import kotlinx.coroutines.async

class SchoolDetailsViewModel(
    private val scoresRepository: ScoresDataSource,
    private val schoolsRepo: SchoolsRepositoryContract
) : ViewModel() {

    private val _state = MutableLiveData<SchoolDetailsState>()
    val state: LiveData<SchoolDetailsState> get() = _state

    fun handleInit() {
        schoolsRepo.getSelectedSchool()?.let { school ->
            viewModelScope.async {
                try {
                    _state.postValue(
                        SchoolDetailsState.Success(
                            school,
                            scoresRepository.fetchScoresForSchool(school.dbn)
                        )
                    )
                } catch (e: Throwable) {
                    e.printStackTrace()
                    _state.postValue(SchoolDetailsState.Error(R.string.scores_error))
                }
            }
        }
    }

}