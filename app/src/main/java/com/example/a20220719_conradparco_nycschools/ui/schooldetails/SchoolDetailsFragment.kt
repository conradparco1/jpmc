package com.example.a20220719_conradparco_nycschools.ui.schooldetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.a20220719_conradparco_nycschools.R
import com.example.a20220719_conradparco_nycschools.data.School
import com.example.a20220719_conradparco_nycschools.data.Score
import com.example.a20220719_conradparco_nycschools.databinding.FragmentSchoolDetailsBinding
import com.example.a20220719_conradparco_nycschools.utils.gone
import com.example.a20220719_conradparco_nycschools.utils.show
import com.google.android.material.snackbar.Snackbar

class SchoolDetailsFragment : Fragment() {

    private var _binding: FragmentSchoolDetailsBinding? = null
    private val binding get() = _binding!!
    private val viewModel: SchoolDetailsViewModel by lazy {
        ViewModelProvider(this, SchoolDetailsViewModelFactory())[SchoolDetailsViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSchoolDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observe()
        viewModel.handleInit()
    }

    private fun observe() {
        viewModel.state.observe(viewLifecycleOwner) {
            when (it) {
                is SchoolDetailsState.None -> {}
                is SchoolDetailsState.Loading -> showLoading()
                is SchoolDetailsState.Success -> handleSuccess(it)
                is SchoolDetailsState.Error -> handleError(it)
            }
        }
    }

    private fun handleSuccess(success: SchoolDetailsState.Success) {
        stopShowingLoading()
        binding.toolbar.title = success.school.schoolName
        setOverview(success.school)
        val scores = success.scores
        when {
            scores != null -> {
                setMath(scores)
                setReading(scores)
                setWriting(scores)
            }
            else -> setNoScoresAvailable()
        }
    }

    private fun setNoScoresAvailable() {
        binding.satMath.setText(R.string.no_scores_avilable)
        binding.satReading.setText(R.string.no_scores_avilable)
        binding.satWriting.setText(R.string.no_scores_avilable)
    }

    private fun setOverview(school: School) {
        binding.overview.text = school.overviewParagraph ?: ""
    }

    private fun setMath(scores: Score) {
        when {
            scores.averageMathScore.equals(
                "s",
                true
            ) -> binding.satMath.setText(R.string.no_scores_avilable)
            else -> binding.satMath.setText(scores.averageMathScore)
        }
    }

    private fun setReading(scores: Score) {
        when {
            scores.averageCriticalReadingScore.equals(
                "s",
                true
            ) -> binding.satReading.setText(R.string.no_scores_avilable)
            else -> binding.satReading.setText(scores.averageCriticalReadingScore)
        }
    }

    private fun setWriting(scores: Score) {
        when {
            scores.averageWritingScore.equals(
                "s",
                true
            ) -> binding.satWriting.setText(R.string.no_scores_avilable)
            else -> binding.satWriting.setText(scores.averageWritingScore)
        }
    }

    private fun handleError(error: SchoolDetailsState.Error) {
        stopShowingLoading()
        Snackbar.make(
            binding.root,
            error.message ?: R.string.scores_error,
            Snackbar.LENGTH_LONG
        ).apply {
            setAction(R.string.retry) {
                viewModel.handleInit()
            }
            show()
        }
    }

    private fun showLoading() {
        binding.progressBar.show()
    }

    private fun stopShowingLoading() {
        binding.progressBar.gone()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}