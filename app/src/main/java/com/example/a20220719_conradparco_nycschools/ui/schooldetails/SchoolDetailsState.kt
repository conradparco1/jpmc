package com.example.a20220719_conradparco_nycschools.ui.schooldetails

import com.example.a20220719_conradparco_nycschools.data.School
import com.example.a20220719_conradparco_nycschools.data.Score

// This state will represent the possible states of the SchoolDetailsFragment
sealed class SchoolDetailsState {
    object None : SchoolDetailsState()
    object Loading : SchoolDetailsState()
    data class Error(val message: Int? = null): SchoolDetailsState()
    data class Success(val school: School, val scores: Score? = null) : SchoolDetailsState()
}