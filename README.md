## NYC Schools app
This is the interview code test I've completed for Photon/JPMC

## Architecture
This app is architected into MVVM. I would have done multimodule but I didn't have enough time.
 - When I have more time, I need to add dagger for proper Dependency Injection

### Testing
The only tests I wrote were for SchoolRepository because I didn't have time to write the other ones